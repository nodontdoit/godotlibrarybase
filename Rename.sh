echo "Please enter the new name:"
read Name
sed --in-place "s/GodotLibraryBase/${Name}/g" GodotLibraryBase.sln
mv GodotLibraryBase.sln ${Name}.sln
sed --in-place "s/GodotLibraryBase/${Name}/g" GodotLibraryBase.csproj
mv GodotLibraryBase.csproj ${Name}.csproj
sed --in-place "s/GodotLibraryBase/${Name}/g" project.godot
rm Rename.sh